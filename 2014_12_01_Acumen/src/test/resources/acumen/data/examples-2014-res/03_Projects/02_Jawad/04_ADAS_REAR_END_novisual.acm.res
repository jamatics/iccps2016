#0 {
  className = Main,
  controller = #0.3,
  mechanics = #0.1,
  sens = #0.2,
  simulator = #0.0
}
#0.0 { className = Simulator, time = 0.000000 }
#0.1 {
  V = 0,
  V0 = 0,
  className = VehicleMechanics,
  th = 0,
  th' = 0,
  th0 = 0,
  w = 0,
  w0 = 0,
  x = 0,
  x' = 0,
  x0 = 0,
  y = 0,
  y' = 0,
  y0 = 0
}
#0.2 {
  className = Sensor,
  s0 = 0,
  s1 = 0,
  s2 = 0,
  sth = 0,
  sx = 0,
  sy = 0,
  th = 0,
  th0 = 0,
  x = 0,
  x0 = 0,
  y = 0,
  y0 = 0
}
#0.3 {
  T_th = 0.785398,
  T_x = 30,
  T_y = 40,
  V = 0,
  V0 = 0,
  className = Control,
  eth = 0,
  ex = 0,
  ey = 0,
  k_a = 8,
  k_b = -1.500000,
  k_v = 3,
  s0 = 0,
  s1 = 0,
  s2 = 0,
  t = 0,
  t' = 0,
  t_c = 0.010000,
  th = 0,
  w = 0,
  w0 = 0,
  x = 0,
  y = 0
}
------------------------------0
#0 {
  className = Main,
  controller = #0.3,
  mechanics = #0.1,
  sens = #0.2,
  simulator = #0.0
}
#0.0 { className = Simulator, time = 0.020000 }
#0.1 {
  V = 0,
  V0 = 0,
  className = VehicleMechanics,
  th = 0,
  th' = 0,
  th0 = 0,
  w = 0,
  w0 = 0,
  x = 0.000000,
  x' = 0.000000,
  x0 = 0,
  y = 0.000000,
  y' = 0.000000,
  y0 = 0
}
#0.2 {
  className = Sensor,
  s0 = 0,
  s1 = 0,
  s2 = 0,
  sth = 0,
  sx = 0,
  sy = 0,
  th = 0,
  th0 = 0,
  x = 0.000000,
  x0 = 0,
  y = 0.000000,
  y0 = 0
}
#0.3 {
  T_th = 0.785398,
  T_x = 30,
  T_y = 40,
  V = 0.000000,
  V0 = 0,
  className = Control,
  eth = 0.785398,
  ex = 10.000000,
  ey = 40,
  k_a = 8,
  k_b = -1.500000,
  k_v = 3,
  s0 = 0,
  s1 = 0,
  s2 = 0,
  t = 0,
  t' = 1,
  t_c = 0.010000,
  th = 0,
  w = 0.000000,
  w0 = 0,
  x = 0,
  y = 0
}
------------------------------3
#0 {
  className = Main,
  controller = #0.3,
  mechanics = #0.1,
  sens = #0.2,
  simulator = #0.0
}
#0.0 { className = Simulator, time = 0.040000 }
#0.1 {
  V = 0.000000,
  V0 = 0,
  className = VehicleMechanics,
  th = 0,
  th' = 0.000000,
  th0 = 0,
  w = 0.000000,
  w0 = 0,
  x = 0.000000,
  x' = 0.000000,
  x0 = 0,
  y = 0.000000,
  y' = 0.000000,
  y0 = 0
}
#0.2 {
  className = Sensor,
  s0 = 0,
  s1 = 0,
  s2 = 0,
  sth = 0,
  sx = 0.000000,
  sy = 0.000000,
  th = 0,
  th0 = 0,
  x = 0.000000,
  x0 = 0,
  y = 0.000000,
  y0 = 0
}
#0.3 {
  T_th = 0.785398,
  T_x = 30,
  T_y = 40,
  V = 123.693169,
  V0 = 0,
  className = Control,
  eth = 0.785398,
  ex = 10.000000,
  ey = 40.000000,
  k_a = 8,
  k_b = -1.500000,
  k_v = 3,
  s0 = 0,
  s1 = 0,
  s2 = 0,
  t = 0,
  t' = 1,
  t_c = 0.010000,
  th = 0,
  w = 8.610483,
  w0 = 0,
  x = 0.000000,
  y = 0.000000
}
------------------------------6
#0 {
  className = Main,
  controller = #0.3,
  mechanics = #0.1,
  sens = #0.2,
  simulator = #0.0
}
#0.0 { className = Simulator, time = 0.060000 }
#0.1 {
  V = 123.693169,
  V0 = 0,
  className = VehicleMechanics,
  th = 0,
  th' = 8.610483,
  th0 = 0,
  w = 8.610483,
  w0 = 0,
  x = 1.235404,
  x' = 123.693169,
  x0 = 0,
  y = 0.053220,
  y' = 0.000000,
  y0 = 0
}
#0.2 {
  className = Sensor,
  s0 = 0,
  s1 = 0,
  s2 = 0,
  sth = 0,
  sx = 0.000000,
  sy = 0.000000,
  th = 0,
  th0 = 0,
  x = 0.000000,
  x0 = 0,
  y = 0.000000,
  y0 = 0
}
#0.3 {
  T_th = 0.785398,
  T_x = 30,
  T_y = 40,
  V = 123.693169,
  V0 = 0,
  className = Control,
  eth = 0.785398,
  ex = 10.000000,
  ey = 40.000000,
  k_a = 8,
  k_b = -1.500000,
  k_v = 3,
  s0 = 0,
  s1 = 0,
  s2 = 0,
  t = 0,
  t' = 1,
  t_c = 0.010000,
  th = 0,
  w = 8.610483,
  w0 = 0,
  x = 0.000000,
  y = 0.000000
}
------------------------------9
#0 {
  className = Main,
  controller = #0.3,
  mechanics = #0.1,
  sens = #0.2,
  simulator = #0.0
}
#0.0 { className = Simulator, time = 5.010000 }
#0.1 {
  V = 63.636516,
  V0 = 0,
  className = VehicleMechanics,
  th = 0,
  th' = 10.932087,
  th0 = 0,
  w = 10.946818,
  w0 = 0,
  x = 453.372429,
  x' = 63.821726,
  x0 = 0,
  y = 21.467353,
  y' = 0.000000,
  y0 = 0
}
#0.2 {
  className = Sensor,
  s0 = 0,
  s1 = 0,
  s2 = 0,
  sth = 0,
  sx = 452.098535,
  sy = 21.397652,
  th = 0,
  th0 = 0,
  x = 452.735482,
  x0 = 0,
  y = 21.432503,
  y0 = 0
}
#0.3 {
  T_th = 0.785398,
  T_x = 30,
  T_y = 40,
  V = 63.636516,
  V0 = 0,
  className = Control,
  eth = 0.785398,
  ex = 10.000000,
  ey = 18.637253,
  k_a = 8,
  k_b = -1.500000,
  k_v = 3,
  s0 = 0,
  s1 = 0,
  s2 = 0,
  t = 0.010000,
  t' = 1,
  t_c = 0.010000,
  th = 0,
  w = 10.946818,
  w0 = 0,
  x = 452.098535,
  y = 21.397652
}
------------------------------751
