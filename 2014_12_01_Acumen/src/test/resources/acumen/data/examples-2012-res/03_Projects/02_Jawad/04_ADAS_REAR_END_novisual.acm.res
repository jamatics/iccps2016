#0 {
  className = Main,
  controller = #0.3,
  mechanics = #0.1,
  sens = #0.2,
  simulator = #0.0
}
#0.0 { className = Simulator, time = 0.000000 }
#0.1 {
  V = 0,
  V0 = 0,
  className = VehicleMechanics,
  th = 0,
  th' = 0,
  th0 = 0,
  w = 0,
  w0 = 0,
  x = 0,
  x' = 0,
  x0 = 0,
  y = 0,
  y' = 0,
  y0 = 0
}
#0.2 {
  className = Sensor,
  s0 = 0,
  s1 = 0,
  s2 = 0,
  sth = 0,
  sx = 0,
  sy = 0,
  th = 0,
  th0 = 0,
  x = 0,
  x0 = 0,
  y = 0,
  y0 = 0
}
#0.3 {
  T_th = 0.785398,
  T_x = 30,
  T_y = 40,
  V = 0,
  V0 = 0,
  className = Control,
  eth = 0,
  ex = 0,
  ey = 0,
  k_a = 8,
  k_b = -1.500000,
  k_v = 3,
  s0 = 0,
  s1 = 0,
  s2 = 0,
  t = 0,
  t' = 0,
  t_c = 0.010000,
  th = 0,
  w = 0,
  w0 = 0,
  x = 0,
  y = 0
}
------------------------------0
#0 {
  className = Main,
  controller = #0.3,
  mechanics = #0.1,
  sens = #0.2,
  simulator = #0.0
}
#0.0 { className = Simulator, time = 0.020000 }
#0.1 {
  V = 0,
  V0 = 0,
  className = VehicleMechanics,
  th = 0.000000,
  th' = 0,
  th0 = 0,
  w = 0,
  w0 = 0,
  x = 0.000000,
  x' = 0.000000,
  x0 = 0,
  y = 0.000000,
  y' = 0.000000,
  y0 = 0
}
#0.2 {
  className = Sensor,
  s0 = 0,
  s1 = 0,
  s2 = 0,
  sth = 0.000000,
  sx = 0.000000,
  sy = 0.000000,
  th = 0.000000,
  th0 = 0,
  x = 0.000000,
  x0 = 0,
  y = 0.000000,
  y0 = 0
}
#0.3 {
  T_th = 0.785398,
  T_x = 30,
  T_y = 40,
  V = 123.693169,
  V0 = 0,
  className = Control,
  eth = 0.785398,
  ex = 10.000000,
  ey = 40.000000,
  k_a = 8,
  k_b = -1.500000,
  k_v = 3,
  s0 = 0,
  s1 = 0,
  s2 = 0,
  t = 0,
  t' = 1,
  t_c = 0.010000,
  th = 0.000000,
  w = 8.610483,
  w0 = 0,
  x = 0.000000,
  y = 0.000000
}
------------------------------3
#0 {
  className = Main,
  controller = #0.3,
  mechanics = #0.1,
  sens = #0.2,
  simulator = #0.0
}
#0.0 { className = Simulator, time = 0.040000 }
#0.1 {
  V = 123.693169,
  V0 = 0,
  className = VehicleMechanics,
  th = 0.172210,
  th' = 8.610483,
  th0 = 0,
  w = 8.610483,
  w0 = 0,
  x = 2.469281,
  x' = 123.234919,
  x0 = 0,
  y = 0.106374,
  y' = 10.637423,
  y0 = 0
}
#0.2 {
  className = Sensor,
  s0 = 0,
  s1 = 0,
  s2 = 0,
  sth = 0.086105,
  sx = 1.236932,
  sy = 0.000000,
  th = 0.086105,
  th0 = 0,
  x = 1.236932,
  x0 = 0,
  y = 0.000000,
  y0 = 0
}
#0.3 {
  T_th = 0.785398,
  T_x = 30,
  T_y = 40,
  V = 123.693169,
  V0 = 0,
  className = Control,
  eth = 0.699293,
  ex = 10.000000,
  ey = 40.000000,
  k_a = 8,
  k_b = -1.500000,
  k_v = 3,
  s0 = 0,
  s1 = 0,
  s2 = 0,
  t = 0,
  t' = 1,
  t_c = 0.010000,
  th = 0.086105,
  w = 7.921644,
  w0 = 0,
  x = 1.236932,
  y = 0.000000
}
------------------------------6
#0 {
  className = Main,
  controller = #0.3,
  mechanics = #0.1,
  sens = #0.2,
  simulator = #0.0
}
#0.0 { className = Simulator, time = 0.060000 }
#0.1 {
  V = 123.693169,
  V0 = 0,
  className = VehicleMechanics,
  th = 0.330643,
  th' = 7.921644,
  th0 = 0,
  w = 7.921644,
  w0 = 0,
  x = 4.885957,
  x' = 119.804084,
  x0 = 0,
  y = 0.626065,
  y' = 30.773063,
  y0 = 0
}
#0.2 {
  className = Sensor,
  s0 = 0,
  s1 = 0,
  s2 = 0,
  sth = 0.251426,
  sx = 3.687917,
  sy = 0.318335,
  th = 0.251426,
  th0 = 0,
  x = 3.687917,
  x0 = 0,
  y = 0.318335,
  y0 = 0
}
#0.3 {
  T_th = 0.785398,
  T_x = 30,
  T_y = 40,
  V = 122.766898,
  V0 = 0,
  className = Control,
  eth = 0.533972,
  ex = 10.000000,
  ey = 39.681665,
  k_a = 8,
  k_b = -1.500000,
  k_v = 3,
  s0 = 0,
  s1 = 0,
  s2 = 0,
  t = 0,
  t' = 1,
  t_c = 0.010000,
  th = 0.251426,
  w = 6.616997,
  w0 = 0,
  x = 3.687917,
  y = 0.318335
}
------------------------------9
#0 {
  className = Main,
  controller = #0.3,
  mechanics = #0.1,
  sens = #0.2,
  simulator = #0.0
}
#0.0 { className = Simulator, time = 5.010000 }
#0.1 {
  V = 32.759198,
  V0 = 0,
  className = VehicleMechanics,
  th = 3.141593,
  th' = 0.000000,
  th0 = 0,
  w = 0.000000,
  w0 = 0,
  x = -111.174881,
  x' = -32.759198,
  x0 = 0,
  y = 44.386407,
  y' = -0.000000,
  y0 = 0
}
#0.2 {
  className = Sensor,
  s0 = 0,
  s1 = 0,
  s2 = 0,
  sth = 3.141593,
  sx = -110.847289,
  sy = 44.386407,
  th = 3.141593,
  th0 = 0,
  x = -110.847289,
  x0 = 0,
  y = 44.386407,
  y0 = 0
}
#0.3 {
  T_th = 0.785398,
  T_x = 30,
  T_y = 40,
  V = 32.759198,
  V0 = 0,
  className = Control,
  eth = -2.356194,
  ex = 10.000000,
  ey = -4.386407,
  k_a = 8,
  k_b = -1.500000,
  k_v = 3,
  s0 = 0,
  s1 = 0,
  s2 = 0,
  t = 0.010000,
  t' = 1,
  t_c = 0.010000,
  th = 3.141593,
  w = 0.000000,
  w0 = 0,
  x = -110.847289,
  y = 44.386407
}
------------------------------751
